/* main.c simple program to test assembler program */

#include <stdio.h>

extern unsigned long long int multiply2(unsigned int a, unsigned int b);
extern unsigned long long int multiply3(unsigned int a, unsigned int b);
extern unsigned long long int powers(unsigned int n, unsigned int m);

void assignment2(void);
void assignment3(void);
void assignment4(void);

int main(void) {
    printf("Luuk Roozen\n");
    assignment3();
    assignment4();
    assignment2();


    return 0;
}

void assignment2(void) {
    unsigned long long int b = 0;
    printf("ASSIGNMENT 2\n");
	b = multiply2(0, 0);
    printf("Result of 0 x 0 = 0, calculated: %lu\n", b);

    b = multiply2(0, 1);
    printf("Result of 0 x 1 = 0, calculated: %lu\n", b);

    b = multiply2(1, 0);
    printf("Result of 1 x 0 = 0, calculated: %lu\n", b);

    b = multiply2(1, 1);
    printf("Result of 1 x 1 = 1, calculated: %lu\n", b);

    b = multiply2(1, 4294967295);
    printf("Result of 1 x 4294967295 = 4294967295, calculated: %lu\n", b);

    b = multiply2(4294967295, 1);
    printf("Result of 4294967295 x 1 = 4294967295, calculated: %lu\n", b);

    b = multiply2(4294967295, 4294967295);
    printf("Result of 4294967295 x 4294967295 = 18446744065119617025, calculated: %lu\n", b);
}

void assignment3(void) {
    unsigned long long int b = 0;
    printf("ASSIGNMENT 3\n");
	b = multiply3(0, 0);
    printf("Result of 0 x 0 = 0, calculated: %lu\n", b);

    b = multiply3(0, 1);
    printf("Result of 0 x 1 = 0, calculated: %lu\n", b);

    b = multiply3(1, 0);
    printf("Result of 1 x 0 = 0, calculated: %lu\n", b);

    b = multiply3(1, 1);
    printf("Result of 1 x 1 = 1, calculated: %lu\n", b);

    b = multiply3(1, 4294967295);
    printf("Result of 1 x 4294967295 = 4294967295, calculated: %lu\n", b);

    b = multiply3(4294967295, 1);
    printf("Result of 4294967295 x 1 = 4294967295, calculated: %lu\n", b);

    b = multiply3(4294967295, 4294967295);
    printf("Result of 4294967295 x 4294967295 = 18446744065119617025, calculated: %lu\n", b);
}

void assignment4(void) {
    unsigned long long int b = 0;
    printf("ASSIGNMENT 4\n");
    // powers(m,n) -> n^m
	b = powers(5, 0);
    printf("Result of 5 ^ 0 = 1, calculated: %lu\n", b);

	b = powers(0, 0);
    printf("Result of 0 ^ 0 = math error, calculated: %lu\n", b);

	b = powers(0, 1);
    printf("Result of 1 ^ 0 = 1, calculated: %lu\n", b);

	b = powers(0, 5);
    printf("Result of 5 ^ 0 = 1, calculated: %lu\n", b);

	b = powers(1, 5);
    printf("Result of 5 ^ 1 = 5, calculated: %lu\n", b);

	b = powers(2, 5);
    printf("Result of 5 ^ 2 = 25, calculated: %lu\n", b);

	b = powers(5, 5);
    printf("Result of 5 ^ 2 = 3125, calculated: %lu\n", b);

}
